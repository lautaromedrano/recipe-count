<?php

namespace module\command;

use Exception;
use module\db\Connection;
use module\models\Delivery;
use module\models\Recipe;
use module\parser\JsonParser;

class Parse extends AbstractCommand
{
    private string $fileName;
    private array $cache;

    public function __construct(Connection $connection, $fileName)
    {
        $this->fileName = $fileName;
        $this->cache    = [];

        parent::__construct($connection);
    }

    /**
     * @throws Exception
     */
    protected function doExecute()
    {
        $parser = new JsonParser($this->fileName);

        while (($record = $parser->getNext())) {
            if (array_key_exists($record['recipe'], $this->cache)) {
                $recipe = $this->cache[$record['recipe']];
            } else {
                $recipe = new Recipe($record['recipe']);
                $recipe->save($this->getConnection());
                $this->cache[$recipe->getName()] = $recipe;
            }

            // Better to do a regexp match
            $time = explode(" ", $record['delivery']);
            $delivery = new Delivery($recipe->getId(), $record['postcode'], $time[0], $time[1], $time[3]);
            $delivery->save($this->getConnection());
        }

        Delivery::forceSave($this->getConnection());
    }

    /**
     * @throws Exception
     */
    protected function validate()
    {
        if (!$this->fileName) {
            throw new Exception("Invalid file to parse");
        }
    }
}
