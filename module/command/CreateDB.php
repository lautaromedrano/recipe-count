<?php

namespace module\command;

use Exception;

class CreateDB extends AbstractCommand
{
    const TABLES_FILENAME = '/../db/resources/tables.sql';

    /**
     * @throws Exception
     */
    protected function doExecute()
    {
        $creates = file_get_contents(dirname(__FILE__) . self::TABLES_FILENAME);
        $this->getConnection()->execute($creates);
    }

    protected function validate()
    {
        // Nothing to do here
    }
}
