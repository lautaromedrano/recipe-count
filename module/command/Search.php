<?php

namespace module\command;

use Exception;
use module\db\Connection;
use module\db\query\QueryFactory;
use module\traits\ConvertTo24Hour;

class Search extends AbstractCommand
{
    use ConvertTo24Hour;

    private QueryFactory $queryFactory;

    private string $postcode;
    private string $startTime;
    private string $endTime;
    private array  $matchWords;

    public function __construct(Connection $connection, QueryFactory $queryFactory, string $postcode, string $startTime, string $endTime, string ...$matchWords)
    {
        $this->queryFactory = $queryFactory;

        $this->postcode   = $postcode;
        $this->startTime  = $startTime;
        $this->endTime    = $endTime;
        $this->matchWords = $matchWords;

        parent::__construct($connection);
    }

    protected function doExecute()
    {
        $countUniqueRecipeNames    = $this->queryFactory->make('countUniqueRecipeNames')->execute();
        $countRecipeOccurrences    = $this->queryFactory->make('countRecipeOccurrences')->execute();
        $mostDeliveredPostCode     = $this->queryFactory->make('mostDeliveredPostCode')->execute();
        $countDeliveriesToPostCode = $this->queryFactory->make('countDeliveriesToPostCode', $this->postcode, $this->get24HourTime($this->startTime), $this->get24HourTime($this->endTime))->execute();
        $matchRecipeName           = $this->queryFactory->make('matchRecipeName', $this->matchWords)->execute();

        echo json_encode(
            [
                "unique_recipe_count" => $countUniqueRecipeNames[0][0],
                "count_per_recipe" => $this->getOutputForRecipeCount($countRecipeOccurrences),
                "busiest_postcode" => [
                    "postcode" => $mostDeliveredPostCode[0][0],
                    "delivery_count" => $mostDeliveredPostCode[0][1]
                ],
                "count_per_postcode_and_time" => [
                    "postcode" => $this->postcode,
                    "from" => $this->startTime,
                    "to" => $this->endTime,
                    "delivery_count" => $countDeliveriesToPostCode[0][0]
                ],
                "match_by_name" => array_map(
                    function ($row) {
                        return $row[0];
                    },
                    $matchRecipeName
                )
            ]
        );
    }

    /**
     * @throws Exception
     */
    protected function validate()
    {
        if (count($this->matchWords) === 0) {
            throw new Exception("At least one search word should be provided.");
        }
    }

    private function getOutputForRecipeCount(array $countRecipeOccurrences): array
    {
        $output = [];

        foreach ($countRecipeOccurrences as $recipe) {
            $output[] = [
                "recipe" => $recipe[0],
                "count" => $recipe[1]
            ];
        }

        return $output;
    }
}
