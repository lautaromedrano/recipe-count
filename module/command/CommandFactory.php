<?php

namespace module\command;

use Exception;
use module\db\Connection;
use module\db\query\QueryFactory;

class CommandFactory
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws Exception
     */
    public function make($arguments): AbstractCommand
    {
        $command = $arguments[1];

        switch ($command) {
            case 'create-db':
                return new CreateDB($this->connection);
            case 'parse':
                return new Parse($this->connection, $arguments[2]);
            case 'search':
                $queryFactory = new QueryFactory($this->connection);
                return new Search($this->connection, $queryFactory, ...array_slice($arguments, 2));
            default:
                throw new Exception("Invalid command");
        }
    }
}
