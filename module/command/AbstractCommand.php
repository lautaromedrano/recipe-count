<?php

namespace module\command;

use module\db\Connection;

abstract class AbstractCommand
{
    private Connection $connection;

    abstract protected function doExecute();
    abstract protected function validate();

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    final public function execute()
    {
        $this->validate();
        $this->doExecute();
    }

    protected function getConnection(): Connection
    {
        return $this->connection;
    }
}
