<?php

namespace module\db;

use Exception;
use mysqli;

class Connection
{
    private mysqli $mysqli;

    public function __construct()
    {
        $host = $_ENV["MYSQL_HOST"] ?? "db";
        $user = $_ENV["MYSQL_USER"] ?? "user";
        $pass = $_ENV["MYSQL_PASS"] ?? "password";
        $db = $_ENV["MYSQL_DB"] ?? "app";
        $port = $_ENV["MYSQL_PORT"] ?? "3306";

        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->mysqli = new mysqli($host, $user, $pass, $db, $port);
    }

    /**
     * @throws Exception
     */
    public function execute(string $query)
    {
        if (!$this->mysqli->multi_query($query)) {
            throw new Exception($this->mysqli->error);
        }
    }

    public function insert(string $query, string $types, array $params): int
    {
        $statement = $this->mysqli->prepare($query);
        $statement->bind_param($types, ...$params);
        $statement->execute();

        $id = $statement->insert_id;

        $statement->close();

        return $id;
    }

    public function query(string $query, string $types, array $params): mixed
    {
        $statement = $this->mysqli->prepare($query);

        if ($types !== '') {
            $statement->bind_param($types, ...$params);
        }

        $statement->execute();
        $result = $statement->get_result()->fetch_all();

        $statement->close();

        return $result;
    }
}
