DROP TABLE IF EXISTS `app`.`deliveries`;
DROP TABLE IF EXISTS `app`.`recipes`;
CREATE TABLE `app`.`recipes` (
    `id` INT unsigned NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(256) NOT NULL,
    UNIQUE KEY `unique_recipes_name` (`name`),
    FULLTEXT `fulltext_recipes_name` (`name`),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;
CREATE TABLE `app`.`deliveries` (
    `id` INT unsigned NOT NULL AUTO_INCREMENT,
    `postcode` VARCHAR(16) NOT NULL,
    `day` VARCHAR(10) NOT NULL,
    `start_time` INT unsigned NOT NULL,
    `end_time` INT NOT NULL,
    `recipe_id` INT unsigned NOT NULL,
    KEY `deliveries_postcode` (`postcode`),
    CONSTRAINT `foreign_recipes_id` FOREIGN KEY (recipe_id) REFERENCES recipes(id),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;
