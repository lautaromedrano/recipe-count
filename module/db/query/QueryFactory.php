<?php

namespace module\db\query;

use Exception;
use module\db\Connection;

class QueryFactory
{
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @throws Exception
     */
    public function make(string $queryType, mixed ...$queryArgs): AbstractQuery
    {
        switch ($queryType) {
            case 'countDeliveriesToPostCode':
                return new CountDeliveriesToPostCode($this->connection, ...$queryArgs);
            case 'countRecipeOccurrences':
                return new CountRecipeOccurrences($this->connection);
            case 'countUniqueRecipeNames':
                return new CountUniqueRecipeNames($this->connection);
            case 'matchRecipeName':
                return new MatchRecipeName($this->connection, ...$queryArgs);
            case 'mostDeliveredPostCode':
                return new MostDeliveredPostCode($this->connection);
            default:
                throw new Exception("Invalid query");
        }
    }
}
