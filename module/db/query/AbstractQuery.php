<?php

namespace module\db\query;

use module\db\Connection;

abstract class AbstractQuery
{
    private Connection $connection;

    abstract protected  function get(): string;
    abstract protected function getParamTypes(): string;
    abstract protected function getParams(): array;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function execute(): mixed
    {
        return $this->connection->query($this->get(), $this->getParamTypes(), $this->getParams());
    }
}
