<?php

namespace module\db\query;

class CountUniqueRecipeNames extends AbstractQuery
{
    protected  function get(): string
    {
        return "
            SELECT count(1)
            FROM   recipes
        ";
    }

    protected function getParamTypes(): string
    {
        return '';
    }

    protected function getParams(): array
    {
        return [];
    }
}
