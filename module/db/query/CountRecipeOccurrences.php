<?php

namespace module\db\query;

class CountRecipeOccurrences extends AbstractQuery
{
    protected  function get(): string
    {
        return "
            SELECT    name, COUNT(1) as occurrences
            FROM      recipes
            JOIN      deliveries ON recipes.id = deliveries.recipe_id
            GROUP BY  recipes.id, name
            ORDER BY  name
        ";
    }

    protected function getParamTypes(): string
    {
        return '';
    }

    protected function getParams(): array
    {
        return [];
    }
}
