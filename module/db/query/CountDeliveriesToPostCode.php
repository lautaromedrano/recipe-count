<?php

namespace module\db\query;

use module\db\Connection;

class CountDeliveriesToPostCode extends AbstractQuery
{
    private int $postCode;
    private int $startTime;
    private int $endTime;

    public function __construct(Connection $connection, int $postCode, string $startTime, string $endTime)
    {
        parent::__construct($connection);

        $this->postCode = $postCode;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
    }

    protected  function get(): string
    {
        return "
            SELECT COUNT(1)
            FROM   deliveries
            WHERE  postcode = ?
            AND    start_time >= ?
            AND    end_time <= ?
        ";
    }

    protected function getParamTypes(): string
    {
        return 'sii';
    }

    protected function getParams(): array
    {
        return [
            $this->postCode,
            $this->startTime,
            $this->endTime
        ];
    }
}
