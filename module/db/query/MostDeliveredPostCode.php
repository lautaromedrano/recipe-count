<?php

namespace module\db\query;

class MostDeliveredPostCode extends AbstractQuery
{
    protected  function get(): string
    {
        return "
            SELECT   postcode, COUNT(1) as occurrences
            FROM     deliveries
            GROUP BY postcode
            ORDER BY COUNT(1)
            LIMIT    1
        ";
    }

    protected function getParamTypes(): string
    {
        return '';
    }

    protected function getParams(): array
    {
        return [];
    }
}
