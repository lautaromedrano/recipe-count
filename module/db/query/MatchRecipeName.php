<?php

namespace module\db\query;

use module\db\Connection;

class MatchRecipeName extends AbstractQuery
{
    private array $wordsToMatch;

    public function __construct(Connection $connection, array $wordsToMatch)
    {
        parent::__construct($connection);

        $this->wordsToMatch = $wordsToMatch;
    }

    protected function getParamTypes(): string
    {
        return 's';
    }

    protected  function get(): string
    {
        return "
            SELECT name
            FROM   recipes
            WHERE  MATCH (name) against (?)
        ";
    }

    protected function getParams(): array
    {
        return [join(' ', $this->wordsToMatch)];
    }
}
