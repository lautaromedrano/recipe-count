<?php

namespace module\models;

use module\db\Connection;

class Recipe
{
    const TABLE = 'recipes';

    private string $name;
    private ?int   $id;

    public function __construct(string $name, ?int $id = null)
    {
        $this->name = $name;
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function save(Connection $connection)
    {
        $id = $connection->insert(
            sprintf(
                "
                    INSERT INTO %s (name)
                    VALUES (?)
                ",
                self::TABLE
            ),
            's',
            [$this->name]
        );

        if ($id > 0) {
            $this->id = $id;
        }
    }
}
