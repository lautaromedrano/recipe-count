<?php

namespace module\models;

use Exception;
use module\db\Connection;
use module\traits\ConvertTo24Hour;

class Delivery
{
    use ConvertTo24Hour;

    const MAX_BATCH_SIZE = 50000;
    const TABLE = 'deliveries';

    private static array $batch = [];

    private int    $recipeId;
    private string $postcode;
    private string $day;
    private string $startTime;
    private string $endTime;

    public static function forceSave(Connection $connection)
    {
        $connection->execute(
            sprintf("INSERT INTO %s (postcode, day, start_time, end_time, recipe_id) VALUES %s", self::TABLE, join(',', self::$batch))
        );
        self::$batch = [];
    }

    public function __construct(int $recipeId, string $postcode, string $day, string $startTime, string $endTime)
    {
        $this->recipeId  = $recipeId;
        $this->postcode  = $postcode;
        $this->day       = $day;
        $this->startTime = $this->get24HourTime($startTime);
        $this->endTime   = $this->get24HourTime($endTime);
    }

    public function getRecipeId(): int
    {
        return $this->recipeId;
    }

    public function getPostcode(): string
    {
        return $this->postcode;
    }

    public function getDay(): string
    {
        return $this->day;
    }

    public function getStartTime(): string
    {
        return $this->startTime;
    }

    public function getEndTime(): string
    {
        return $this->endTime;
    }

    /**
     * @throws Exception
     */
    public function save(Connection $connection)
    {
        self::$batch[] = sprintf("('%s', '%s', '%d', '%d', '%d')", $this->postcode, $this->day, $this->startTime, $this->endTime, $this->recipeId);
        if (count(self::$batch) === self::MAX_BATCH_SIZE) {
            $connection->execute(
                sprintf("INSERT INTO %s (postcode, day, start_time, end_time, recipe_id) VALUES %s", self::TABLE, join(',', self::$batch))
            );
            self::$batch = [];
        }
    }
}
