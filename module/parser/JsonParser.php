<?php

namespace module\parser;

use Exception;

class JsonParser
{
    private $fileHandler;

    /**
     * @throws Exception
     */
    public function __construct(string $jsonFileName)
    {
        $this->fileHandler = fopen($jsonFileName, "r");
        if (!$this->fileHandler) {
            throw new Exception("Unable to open recipes file.");
        }

        // Skipping '[' at the beginning of the file
        fgets($this->fileHandler);
    }

    public function getNext(): ?array
    {
        $i = 0;
        $record = '';

        while ($i++ < 5) {
            $line = fgets($this->fileHandler);

            if (!$line) {
                return null;
            }

            $record .= $line;
        }

        // Remove last \n and , to decode well
        $record = trim(trim($record), ',');

        return json_decode($record, true);
    }

    public function __destruct()
    {
        fclose($this->fileHandler);
    }
}
