<?php

namespace module;

use Exception;
use module\command\CommandFactory;
use module\db\Connection;

class Application
{
    private int $argumentsCount;
    private array $arguments;

    /**
     * @throws Exception
     */
    public function __construct($argc, $argv)
    {
        $this->argumentsCount = $argc;
        $this->arguments = $argv;

        $this->validateArguments();
    }

    public function run()
    {
        $connection = new Connection();
        $commandFactory = new CommandFactory($connection);

        $command = $commandFactory->make($this->arguments);
        $command->execute();
    }

    /**
     * @throws Exception
     */
    private function validateArguments()
    {
        if ($this->argumentsCount < 2) {
            $this->printHelp();
            throw new Exception("Too few arguments.");
        }

        $command = $this->arguments[1];
        if (!in_array($command, ['create-db', 'parse', "search"])) {
            $this->printHelp();
            throw new Exception("Invalid command.");
        }
    }

    private function printHelp()
    {
        echo "Usage: index.php <create-db|parse|search>\n";
        echo "=========================================\n";
        echo "       index.php create-db\n";
        echo "       index.php parse <fileName>\n";
        echo "       index.php search <postCode> <startTime> <endTime> [word1, word2...]\n";
    }
}
