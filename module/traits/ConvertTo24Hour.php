<?php

namespace module\traits;

trait ConvertTo24Hour
{
    public function get24HourTime(string $time): int
    {
        $isAM = str_ends_with($time, 'AM');
        if ($isAM) {
            if (str_starts_with($time, '12')) {
                return 0;
            }
            return (int)substr($time, 0, -2);
        } else if (str_starts_with($time, '12')) {
            return 12;
        }
        return ((int) substr($time, 0, -2)) + 12;
    }
}
