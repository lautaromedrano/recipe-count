FROM php:8

WORKDIR /var/www

COPY . .

RUN apt-get update && apt-get install -y git zip unzip zlib1g-dev libzip-dev

RUN docker-php-ext-install mysqli

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN composer install

RUN composer dump-autoload
