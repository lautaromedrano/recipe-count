<?php

use module\command\CommandFactory;
use module\command\CreateDB;
use module\command\Parse;
use module\command\Search;
use module\db\Connection;
use PHPUnit\Framework\TestCase;

class CommandFactoryTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testMake() {
        $connection = $this->createMock(Connection::class);
        $factory = new CommandFactory($connection);

        $createDBCommand = $factory->make(['index.php', 'create-db']);
        $this->assertInstanceOf(CreateDB::class, $createDBCommand);

        $parseCommand = $factory->make(['index.php', 'parse', 'file.json']);
        $this->assertInstanceOf(Parse::class, $parseCommand);

        $searchCommand = $factory->make(['index.php', 'search', 10224, '1AM', '12AM', 'word1']);
        $this->assertInstanceOf(Search::class, $searchCommand);
    }
}
