<?php

use module\parser\JsonParser;
use PHPUnit\Framework\TestCase;

class JsonParserTest extends TestCase
{
    const JSON_CONTENT =
        '[
            {
              "postcode": "10224",
              "recipe": "Creamy Dill Chicken",
              "delivery": "Wednesday 1AM - 7PM"
            },
            {
              "postcode": "10208",
              "recipe": "Speedy Steak Fajitas",
              "delivery": "Thursday 7AM - 5PM"
            }
        ]';
    private string $tmpFilePath;

    public function setUp():void
    {
        parent::setUp();

        $tmpFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . "tmpFile3.json";
        $this->tmpFilePath = $tmpFilePath;
        file_put_contents($tmpFilePath, self::JSON_CONTENT);
    }

    public function tearDown(): void
    {
        unlink($this->tmpFilePath);
    }

    /**
     * @throws Exception
     */
    public function testParserReturnsRecord()
    {
        $jsonParser = new JsonParser($this->tmpFilePath);
        $record = $jsonParser->getNext();

        $this->assertEquals("10224", $record['postcode']);
        $this->assertEquals("Creamy Dill Chicken", $record['recipe']);
        $this->assertEquals("Wednesday 1AM - 7PM", $record['delivery']);
    }

    /**
     * @throws Exception
     */
    public function testParserReturnsNullAtEnd()
    {
        $jsonParser = new JsonParser($this->tmpFilePath);

        $record = $jsonParser->getNext();
        $this->assertNotNull($record);

        $record = $jsonParser->getNext();
        $this->assertNotNull($record);

        $record = $jsonParser->getNext();
        $this->assertNull($record);
    }
}
