<?php

use module\db\Connection;
use module\db\query\CountDeliveriesToPostCode;
use module\db\query\CountRecipeOccurrences;
use module\db\query\CountUniqueRecipeNames;
use module\db\query\MatchRecipeName;
use module\db\query\MostDeliveredPostCode;
use module\db\query\QueryFactory;
use PHPUnit\Framework\TestCase;

class QueryFactoryTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testMake() {
        $connection = $this->createMock(Connection::class);
        $factory = new QueryFactory($connection);

        $countUniqueRecipeNames    =  $factory->make('countUniqueRecipeNames') ;
        $this->assertInstanceOf(CountUniqueRecipeNames::class, $countUniqueRecipeNames);

        $countRecipeOccurrences    =  $factory->make('countRecipeOccurrences') ;
        $this->assertInstanceOf(CountRecipeOccurrences::class, $countRecipeOccurrences);

        $mostDeliveredPostCode     =  $factory->make('mostDeliveredPostCode') ;
        $this->assertInstanceOf(MostDeliveredPostCode::class, $mostDeliveredPostCode);

        $countDeliveriesToPostCode =  $factory->make('countDeliveriesToPostCode', 10224, 1, 12) ;
        $this->assertInstanceOf(CountDeliveriesToPostCode::class, $countDeliveriesToPostCode);

        $matchRecipeName           =  $factory->make('matchRecipeName', ['word1']) ;
        $this->assertInstanceOf(MatchRecipeName::class, $matchRecipeName);
    }
}
