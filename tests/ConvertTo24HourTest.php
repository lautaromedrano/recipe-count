<?php

use module\traits\ConvertTo24Hour;
use PHPUnit\Framework\TestCase;

class ConvertTo24HourTest extends TestCase
{
    public function testAMConversion()
    {
        $trait = $this->getObjectForTrait(ConvertTo24Hour::class);
        $this->assertEquals(1, $trait->get24HourTime('1AM'));
        $this->assertEquals(5, $trait->get24HourTime('5AM'));
        $this->assertEquals(11, $trait->get24HourTime('11AM'));
        $this->assertEquals(0, $trait->get24HourTime('12AM'));
    }

    public function testPMConversion()
    {
        $trait = $this->getObjectForTrait(ConvertTo24Hour::class);
        $this->assertEquals(13, $trait->get24HourTime('1PM'));
        $this->assertEquals(17, $trait->get24HourTime('5PM'));
        $this->assertEquals(23, $trait->get24HourTime('11PM'));
        $this->assertEquals(12, $trait->get24HourTime('12PM'));
    }
}
