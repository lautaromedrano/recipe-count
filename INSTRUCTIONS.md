# Steps to run app

## Setting up docker 

```
# Build containers
docker-compose build

# Keep them running
docker-compose up -d
```

## Run tests

```
docker-compose exec cli php vendor/bin/phpunit
```

## Create db

```
docker-compose exec cli php index.php create-db
```

## Parse file

```
docker compose cp <filename> cli:.
docker-compose exec cli php index.php parse <filename>
```

## Execute searches and get output
```
docker-compose exec cli php index.php search <postcode> <startTime> <endTime> <word1, word2...>
```
