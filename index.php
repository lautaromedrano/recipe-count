<?php

require('vendor/autoload.php');

use module\Application;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$app = new Application($argc, $argv);
$app->run();
